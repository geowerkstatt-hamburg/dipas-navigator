import {expect} from "chai";
import {shallowMount} from "@vue/test-utils";
import FilterPill from "@/reusableComponents/FilterPill.vue";

describe("unit/reusableComponents/FilterPill.spec.js", () => {

  it("renders component with prop option", () => {
    const
      text = "Lorem Ipsum 123",
      wrapper = shallowMount(FilterPill, {
        props: {
          option: {
            name: "Lorem Ipsum 123"
          }
        }
      });

    expect(wrapper.text()).to.include(text);
  });

});
