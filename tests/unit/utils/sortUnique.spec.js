import {expect} from "chai";
import {sortUnique} from "../../../src/utils/sortUnique";

describe("unit/utils/sortUnique", () => {

  it("sorts the data correctly and makes it unique", () => {

    const
      sourceData = [
        {id: 1, name: "Az"},
        {id: 2, name: "aa"},
        {id: 3, name: "AA"}, // <- This should be filtered out, because it's the same as "aa"
        {id: 4, name: "zz"},
        {id: 5, name: "Za"},
        {id: 6, name: "Öa"},
        {id: 7, name: "Öz"}
      ],
      expectedData = [
        {id: 2, name: "aa"},
        {id: 1, name: "Az"},
        {id: 6, name: "Öa"},
        {id: 7, name: "Öz"},
        {id: 5, name: "Za"},
        {id: 4, name: "zz"}
      ];

    let
      processedData = sortUnique(sourceData);

    expect(processedData).to.eql(expectedData);
  });

  it("orders data correctly using a different sortKey", () => {

    const
      sortKey = "title",
      sourceData = [
        {id: 2, title: "World"},
        {id: 1, title: "Hello"}
      ],
      expectedData = [
        {id: 1, title: "Hello"},
        {id: 2, title: "World"}
      ];

    let
      processedData = sortUnique(sourceData, sortKey);

    expect(processedData).to.eql(expectedData);
  });
});
