import {expect} from "chai";
import sinon from "sinon";
import {shallowMount} from "@vue/test-utils";
import FilterElement from "@/basicComponents/FilterElement.vue";
import VueMultiselect from "vue-multiselect";

describe("unit/basicComponents/FilterElement.spec.js", () => {
  let $store;

  beforeEach(() => {
    $store = {
      dispatch: sinon.stub(),
      getters: {
        multiFilterData: function () {
          return {
            districts: [
              {id: 1, name: "districts 1"},
              {id: 2, name: "districts 2"}
            ],
            topics: [
              {id: 1, name: "topics 1"},
              {id: 2, name: "topics 2"}
            ],
            owners: [
              {id: 1, name: "owners 1"},
              {id: 2, name: "owners 2"}
            ],
            years: {
              from: null,
              to: null
            }
          };
        }
      }
    };
  });

  it("renders component and contains VueMultiselect", () => {
    const wrapper = shallowMount(FilterElement, {
      global: {
        mocks: {
          $store,
          $t: (msg) => msg
        }
      }
    });

    expect(wrapper.findComponent(VueMultiselect).exists()).to.be.true;
  });
});
