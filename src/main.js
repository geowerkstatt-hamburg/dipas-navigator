import {createApp} from "vue";
import VueMatomo from "vue-matomo";
import {createI18n} from "vue-i18n";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import bootstrapBreakpoints from "./plugins/bootstrapBreakpoints.js";
import pageTitle from "./plugins/pageTitle.js";
import {FunctionLibrary} from "./plugins/FunctionLibrary.js";

import "bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import "material-design-icons/iconfont/material-icons.css";

function loadLocaleMessages () {
  const locales = require.context("./locales", true, /[A-Za-z0-9-_,\s]+\.json$/i),
    messages = {};

  locales.keys().forEach(key => {
    const matched = key.match(/([A-Za-z0-9-_]+)\./i);

    if (matched && matched.length > 1) {

      const locale = matched[1];

      messages[locale] = locales(key);
    }
  });
  return messages;
}

const i18n = createI18n({
    legacy: false,
    // eslint-disable-next-line
    locale: LOCALE,
    globalInjection: true,
    messages: loadLocaleMessages()
  }),

  app = createApp(App);

app.use(store);
app.use(router);
app.use(i18n);
app.use(bootstrapBreakpoints);
app.use(pageTitle);
app.use(FunctionLibrary);

// eslint-disable-next-line no-undef
if (dipasStatisticsSettings.trackingEnabled === 1) {
  app.use(VueMatomo, {
    // eslint-disable-next-line no-undef
    host: dipasStatisticsSettings.backendUrl,
    // eslint-disable-next-line no-undef
    siteId: dipasStatisticsSettings.siteId
  });

  // eslint-disable-next-line
  window._paq.push(["trackPageView"]);
}

app.mount("#app");

