import {createRouter, createWebHashHistory} from "vue-router";
// Pages
import Overview from "../pages/Overview/Overview.vue";
import ContentPage from "../pages/ContentPage/ContentPage.vue";

const routes = [
    {
      path: "/",
      name: "Overview",
      component: Overview,
      props: true,
      meta: {
        pageClass: "overview"
      },
      children: []
    },
    {
      path: "/privacy",
      name: "Privacy",
      component: ContentPage,
      props: {
        pageid: "privacy"
      },
      meta: {
        pageClass: "privacy"
      },
      children: []
    },
    {
      path: "/accessibility",
      name: "Accessibility",
      component: ContentPage,
      props: {
        pageid: "accessibility"
      },
      meta: {
        pageClass: "accessibility"
      },
      children: []
    },
    {
      path: "/imprint",
      name: "Imprint",
      component: ContentPage,
      props: {
        pageid: "imprint"
      },
      meta: {
        pageClass: "imprint"
      },
      children: []
    },
    {
      path: "/about",
      name: "About",
      component: ContentPage,
      props: {
        pageid: "about"
      },
      meta: {
        pageClass: "about"
      },
      children: []
    }
  ],
  router = createRouter({
    history: createWebHashHistory(),
    routes
  });

export default router;
