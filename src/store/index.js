import {Config} from "../config.js";
import {createStore} from "vuex";
import moment from "moment";
import {funcParamlist} from "./utils/filterMethod";
import {sortUnique} from "@/utils/sortUnique";
import {filterProceedingGeoJson} from "./utils/filterProceedings.js";
import {filterQueryString} from "@/store/utils/filterQueryString";
import {isProxy, toRaw} from "vue";
import cookie from "vue-cookies";

export default createStore({
  state: {
    basePath: "",
    basicConfiguration: undefined,
    cookiesAccepted: Boolean(Object.keys(cookie.get("dipasn") ?? {}).length),
    projectlist: [],
    appointmentlist: [],
    documentations: [],
    proceedingGeoJson: {},
    recentContributions: [],
    mostCommentedContributions: [],
    contentPages: {},
    multiFilter: {
      districts: [],
      topics: [],
      owners: [],
      dateStart: null,
      dateEnd: null
    },
    statisticalData: {
      contribution: {},
      comment: {}
    }
  },
  actions: {
    /**
     * Perform http request and return Promise
     * @param   {this} context this store
     * @param   {object} request request
     * @returns {Promise} result of http request or error
     */
    doRequest (context, request) {
      const requestPath = [];

      if (Config.backend.useDifferentBackendHost) {
        requestPath.push(Config.backend.useSSL ? "https://" : "http://");
        requestPath.push(Config.backend.hostname);
        if (!Config.backend.useSSL && Config.backend.port !== 80 || Config.backend.useSSL && Config.backend.port !== 443) {
          requestPath.push(":" + Config.backend.port);
        }
      }
      requestPath.push(Config.backend.basepath);

      context.dispatch("basePath", requestPath);

      let requestParams = null;

      if (request.method === "POST") {
        requestParams = {
          method: "POST",
          body: JSON.stringify(request.payload)
        };
      }

      return new Promise((resolve, reject) => {
        fetch(
          requestPath.join("") + request.endpoint,
          requestParams
        )
          .then(response => response.json())
          .then(json => resolve(json))
          .catch(error => reject(error));
      });
    },
    basicConfiguration ({dispatch, commit}, lang) {
      dispatch("doRequest", {
        endpoint: lang + "/dipas/navigator/config"
      }).then(response => {
        commit("basicConfiguration", response.config);
      });
    },
    confirmCookies ({dispatch, commit}) {
      dispatch(
        "doRequest",
        {
          endpoint: "dipas/navigator/confirmcookies",
          method: "POST",
          payload: {
            confirmCookies: true
          }
        }
      ).then((response) => {
        commit("setCookiesAccepted", response.status === "success");
      });
    },
    projectlist ({dispatch, commit}) {
      dispatch("doRequest", {
        endpoint: "dipas-pds/projects"
      }).then(FeatureCollection => {
        commit("projectlist", FeatureCollection.features);
      });
    },
    appointmentlist ({dispatch, commit}) {
      dispatch("doRequest", {
        endpoint: "dipas/navigator/appointments"
      }).then(response => {
        commit("appointmentlist", response.appointments);
      });
    },
    documentations ({dispatch, commit}) {
      dispatch("doRequest", {
        endpoint: "dipas/navigator/documentations"
      }).then(response => {
        commit("documentations", response.files);
      });
    },
    async proceedingGeoJson ({dispatch, commit}) {
      const response = await dispatch("doRequest", {
        endpoint: "dipas/navigator/cockpitdatamap"
      });

      return commit("proceedingGeoJson", response);
    },
    basePath ({commit}, baseUrl) {
      commit("basePath", baseUrl);
    },
    recentContributions ({dispatch, commit, state}) {
      dispatch("doRequest", {
        endpoint: "dipas/navigator/recentcontributions" + filterQueryString(state.multiFilter)
      }).then(response => {
        commit("recentContributions", response);
      });
    },
    mostCommentedContributions ({dispatch, commit, state}) {
      dispatch("doRequest", {
        endpoint: "dipas/navigator/mostcommentedcontributions" + filterQueryString(state.multiFilter)
      }).then(response => {
        commit("mostCommentedContributions", response);
      });
    },
    contentPage ({state, dispatch, commit}, page) {
      if (!state.contentPages[page]) {
        dispatch("doRequest", {
          endpoint: "dipas/navigator/page/" + page
        }).then(response => {
          commit("contentPage", {
            page,
            content: response
          });
        });
      }
    },
    statisticalData ({state, dispatch, commit}, {type, forceReload}) {
      if (!Object.keys(state.statisticalData[type]).length || forceReload) {
        dispatch("doRequest", {
          endpoint: "dipas/navigator/" + type + "statistics" + filterQueryString(state.multiFilter)
        }).then(response => {
          commit("statisticalData", {type, data: response[type + "counts"]});
        });
      }
    }
  },
  mutations: {
    basicConfiguration (state, configurationData) {
      state.basicConfiguration = configurationData;
    },
    setCookiesAccepted (state, status) {
      state.cookiesAccepted = status;
    },
    basePath (state, basePath) {
      state.basePath = basePath.join("");
    },
    projectlist (state, projects) {
      state.projectlist = projects;
    },
    appointmentlist (state, appointments) {
      state.appointmentlist = appointments;
    },
    documentations (state, files) {
      state.documentations = files;
    },
    proceedingGeoJson (state, geoJson) {
      state.proceedingGeoJson = geoJson;
    },
    recentContributions (state, recentContributions) {
      state.recentContributions = recentContributions.contributions;
    },
    mostCommentedContributions (state, mostCommentedContributions) {
      state.mostCommentedContributions = mostCommentedContributions.contributions;
    },
    contentPage (state, data) {
      let contentPages = JSON.parse(JSON.stringify(state.contentPages));

      contentPages[data.page] = data.content;
      state.contentPages = contentPages;
    },
    multiFilter (state, multiFilter) {
      state.multiFilter = multiFilter;
    },
    statisticalData (state, payload) {
      state.statisticalData[payload.type] = payload.data;
    }
  },
  getters: {
    basePath (state) {
      return state.basePath;
    },
    configurationData (state) {
      return (key) => {
        // use dot syntax to retrieve nested configuration properties
        // e.g. foo.bar.baz to retrieve foo{ bar: { baz: value } }
        let configPath = key.split("."),
          configuration = state.basicConfiguration;

        // Transform proxy objects in raw data objects if necessary
        if (isProxy(configuration)) {
          configuration = toRaw(configuration);
        }

        // recursively retrieve the desired configuration property (or return undefined if unknown)
        return (function retrieveConfigValue (path, configurationSection) {
          if (path.length > 0 && configurationSection !== undefined) {
            return path.length > 1
              ? retrieveConfigValue(path.slice(1), configurationSection[path.shift()])
              : configurationSection[path.shift()];
          }

          return undefined;
        })(configPath, configuration);
      };
    },
    useMatomo () {
      // eslint-disable-next-line no-undef
      return dipasStatisticsSettings.trackingEnabled === 1;
    },
    cookiesAccepted (state) {
      return state.cookiesAccepted;
    },
    projectlist (state) {

      return funcParamlist(state, state.projectlist, "projectlist");
    },
    projectlistFilters (state) {
      return state.projectlist;
    },
    appointmentlist (state) {
      return funcParamlist(state, state.appointmentlist, "appointments");
    },
    recentContributions (state) {
      return state.recentContributions;
    },
    mostCommentedContributions (state) {
      return state.mostCommentedContributions;
    },
    proceedingByYears (state, getters) {
      const proceedings = [...getters.projectlist],
        proceedingsByYears = {},
        sortedResponse = {};

      proceedings.forEach(proceeding => {
        const yearStart = moment(proceeding.properties.dateStart).format("Y"),
          yearEnd = moment(proceeding.properties.dateEnd).format("Y");

        if (proceedingsByYears[yearStart] === undefined) {
          proceedingsByYears[yearStart] = 0;
        }

        if (proceedingsByYears[yearEnd] === undefined) {
          proceedingsByYears[yearEnd] = 0;
        }

        proceedingsByYears[yearStart]++;
        if (yearStart !== yearEnd) {
          proceedingsByYears[yearEnd]++;
        }
      });

      Object.keys(proceedingsByYears).sort((a, b) => a > b ? 1 : -1).forEach(year => {
        sortedResponse[year] = proceedingsByYears[year];
      });

      return sortedResponse;
    },
    proceedingsByTopics (state, getters) {
      const proceedings = [...getters.projectlist],
        proceedingsByTopics = {},
        sortedResponse = [];

      proceedings.forEach(proceeding => {
        Object.values(proceeding.properties.projectTopics).map(topic => topic.name).forEach(topic => {
          if (proceedingsByTopics[topic] === undefined) {
            proceedingsByTopics[topic] = 0;
          }
          proceedingsByTopics[topic]++;
        });
      });

      // eslint-disable-next-line one-var
      const countTotal = Object.values(proceedingsByTopics).length
        ? Object.values(proceedingsByTopics).reduce((accumulated, current) => accumulated + current)
        : 0;

      Object.entries(proceedingsByTopics).forEach(([topic, count]) => {
        sortedResponse.push({
          topic,
          count,
          percentage: Math.ceil(100 * count / countTotal)
        });
      });

      return sortedResponse.sort((a, b) => a.count < b.count ? 1 : -1);
    },
    proceedingsByDistricts (state, getters) {
      const proceedings = [...getters.projectlist],
        districtColors = {},
        proceedingsByDistricts = {},
        sortedResponse = [];

      proceedings.forEach(proceeding => {
        Object.values(proceeding.properties.dipasMainDistrict).forEach(district => {
          districtColors[district.name] = district.field_color;
        });
      });

      proceedings.forEach(proceeding => {
        Object.values(proceeding.properties.dipasMainDistrict).map(district => district.name).forEach(district => {
          if (proceedingsByDistricts[district] === undefined) {
            proceedingsByDistricts[district] = 0;
          }
          proceedingsByDistricts[district]++;
        });
      });

      // eslint-disable-next-line one-var
      const countTotal = Object.values(proceedingsByDistricts).length
        ? Object.values(proceedingsByDistricts).reduce((accumulated, current) => accumulated + current)
        : 0;

      Object.entries(proceedingsByDistricts).forEach(([district, count]) => {
        sortedResponse.push({
          district,
          color: districtColors[district],
          count,
          percentage: Math.ceil(100 * count / countTotal)
        });
      });

      return sortedResponse.sort((a, b) => a.count < b.count ? 1 : -1);
    },
    documentations (state) {
      const arraySize = 3,
        documentArray = funcParamlist(state, state.documentations, "documentations");

      return documentArray.sort((a, b) => a.upload_date < b.upload_date ? 1 : -1).slice(0, arraySize);
    },
    proceedingGeoJson (state) {
      let filterApiNames = {
        districts: "districts",
        topics: "themes",
        owners: "responsible",
        dateStart: "dateStart",
        dateEnd: "dateEnd"
      };

      return filterProceedingGeoJson(state.proceedingGeoJson, state.multiFilter, filterApiNames);
    },
    contentPage (state) {
      return (page) => {
        return state.contentPages[page]
          ? state.contentPages[page]
          : {};
      };
    },
    isFilterActive (state) {
      const filter = JSON.parse(JSON.stringify(state.multiFilter));

      return Object.values(filter).some((elem) => elem && elem.length > 0);
    },
    multiFilterState (state) {
      return state.multiFilter;
    },
    multiFilterData (state) {
      const
        districts = [],
        topics = [],
        owners = [];

      state.projectlist.forEach(project => {
        // A project without projectOwner is not possible
        if (!Object.keys(project.properties.projectOwner).length) {
          return;
        }

        // districts - API field "project.*.properties.dipasMainDistrict.*"
        Object.values(project.properties.dipasMainDistrict).forEach((item) => {
          districts.push({
            id: item.id,
            name: item.name
          });
        });

        // topics - API field "project.*.properties.projectTopics.*"
        Object.values(project.properties.projectTopics).forEach((item) => {
          topics.push({
            id: item.id,
            name: item.name
          });
        });

        // owners - API field "project.*.properties.projectOwner.*"
        Object.values(project.properties.projectOwner).forEach((item) => {
          owners.push({
            id: item.id,
            name: item.name
          });
        });

      });

      return {
        districts: sortUnique(districts),
        topics: sortUnique(topics),
        owners: sortUnique(owners)
      };
    },
    statisticalData: (state) => (type) => {
      return state.statisticalData[type];
    }
  }
});
