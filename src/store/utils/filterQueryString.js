/**
 * Converts the multiFilter object into a URL query string
 * e.g. ?districts[]=123&districts[]=456...&dateEnd=2023-01-01
 * @param {Object} multiFilter filter configuration
 * @returns {String} URL query string
 */
export function filterQueryString (multiFilter) {

  let params = new URLSearchParams();

  multiFilter.districts.forEach(function (item) {
    params.append("districts[]", item.id);
  });

  multiFilter.topics.forEach(function (item) {
    params.append("topics[]", item.id);
  });

  multiFilter.owners.forEach(function (item) {
    params.append("owners[]", item.id);
  });

  if (multiFilter.dateStart) {
    params.append("dateStart", multiFilter.dateStart);
  }

  if (multiFilter.dateEnd) {
    params.append("dateEnd", multiFilter.dateEnd);
  }

  return `?${new URLSearchParams(params).toString()}`;
}

