/**
 * Converts the filter object into an array of key-value pairs.
 * @param {object} filter - The filter object.
 * @param {object} filterApiNames - The object containing API names for filter categories.
 * @returns {Array} - The array of key-value pairs representing the filter.
 */
function makeFilterArray (filter, filterApiNames) {
  const {dateStart, dateEnd} = filter,
    newFilter = {
      [filterApiNames.districts]: Object.values(filter.districts),
      [filterApiNames.topics]: Object.values(filter.topics),
      [filterApiNames.owners]: Object.values(filter.owners),
      [filterApiNames.dateStart]: [dateStart],
      [filterApiNames.dateEnd]: [dateEnd]
    };

  return Object.entries(newFilter);
}

/**
 * Filters the GeoJSON data based on the provided filter.
 * @param {object} geoJson - The GeoJSON data to filter.
 * @param {object} filter - The filter object.
 * @param {object} filterApiNames - The object containing API names for filter categories.
 * @returns {object} - The filtered GeoJSON data.
 */
export function filterProceedingGeoJson (geoJson, filter, filterApiNames) {
  const filters = makeFilterArray(filter, filterApiNames),
    filteredData = filters.reduce((data, [key, values]) => {
      const valuesString = JSON.stringify(values);

      if (valuesString !== "[null]" && valuesString !== "[]") {
        let result = data.features;

        if (key === "dateStart" || key === "dateEnd") {
          result = result.filter((el) => {
            const dateStart = new Date(el.properties.dateStart),
              dateEnd = new Date(el.properties.dateEnd),
              filterEndDate = new Date(filter.dateEnd),
              filterStartDate = new Date(filter.dateStart);

            return filterStartDate <= dateEnd && dateStart <= filterEndDate;
          });
        }
        else {
          result = result.filter((el) => values.some((value) => el.properties[key] && el.properties[key].includes(value.name)));
        }

        return {...data, features: result};
      }

      return data;
    }, {...geoJson});

  return filteredData;
}
