/**
 * Deep filter the data of a state
 * @param  {Object} param path of the state
 * @param  {Object} argv date from filter
 * @param  {String} argw String for query with path of the state
 * @return {paramResult} update data in state
 */
function funcDocumentations (param, argv, argw) {
  const paramResult = param.filter(doc => {
    return argv.some(element => {
      return Object.values(doc[argw]).includes(element);
    });
  });

  return paramResult;
}

/**
 * Deep filter the data of a state
 * @param  {Object} param path of the state
 * @param  {Object} argv date from filter
 * @param  {String} argw String for query with path of the state
 * @return {paramResult} update data in state
 */
function funcProjectlist (param, argv, argw) {
  const paramResult = param.filter(doc => {
    return argv.some(element => {
      return Object.values(doc.properties[argw]).map(key => key.name).includes(element);
    });
  });

  return paramResult;
}

/**
 * Filter the data of a state
 * @param  {Object} state all data from state
 * @param  {Object} params path of the state
 * @param  {String} argv String for query with path of the state
 * @return {filterStateParams} update data in state
 */
export function funcParamlist (state, params, argv) {
  let filterStateParams = "";
  const filterAreas = Object.values(state.multiFilter.districts).map(area => area.name),
    filterTopic = Object.values(state.multiFilter.topics).map(topic => topic.name),
    filterOwner = Object.values(state.multiFilter.owners).map(own => own.name),
    filterStartDate = new Date(state.multiFilter.dateStart),
    filterEndDate = new Date(state.multiFilter.dateEnd);

  filterStateParams = JSON.parse(JSON.stringify(params));

  Object.keys(state.multiFilter).forEach(key => {
    switch (key) {
      case "districts":
        if (filterAreas.length) {
          if (argv === "projectlist") {
            filterStateParams = funcProjectlist(filterStateParams, filterAreas, "dipasMainDistrict");
          }
          else if (argv === "documentations" || argv === "appointments") {
            filterStateParams = funcDocumentations(filterStateParams, filterAreas, "proceedingdistricts");
          }
        }
        break;
      case "topics":
        if (filterTopic.length) {
          if (argv === "projectlist") {
            filterStateParams = funcProjectlist(filterStateParams, filterTopic, "projectTopics");
          }
          else if (argv === "documentations" || argv === "appointments") {
            filterStateParams = funcDocumentations(filterStateParams, filterTopic, "proceedingtopics");
          }
        }
        break;
      case "owners":
        if (filterOwner.length) {
          if (argv === "projectlist") {
            filterStateParams = funcProjectlist(filterStateParams, filterOwner, "projectOwner");
          }
          else if (argv === "documentations" || argv === "appointments") {
            filterStateParams = funcDocumentations(filterStateParams, filterOwner, "proceedingowners");
          }
        }
        break;
      case "dateStart":
        if (state.multiFilter.dateStart) {
          if (argv === "projectlist") {

            filterStateParams = filterStateParams.filter(doc => {
              const endDate = new Date(doc.properties.dateEnd),
                startDate = new Date(doc.properties.dateStart);

              let thisIsObject = "";

              if (filterStartDate <= endDate && startDate <= filterEndDate) {
                thisIsObject = Object.values(doc.properties);
              }
              return thisIsObject;
            });
          }
          else if (argv === "documentations") {

            filterStateParams = filterStateParams.filter(doc => {
              const endDate = new Date(doc.proceedingschedule_end),
                startDate = new Date(doc.proceedingschedule_start);
              let thisIsObject = "";

              if (filterStartDate <= endDate && startDate <= filterEndDate) {

                thisIsObject = Object.values(doc);
              }
              return thisIsObject;
            });
          }
          else if (argv === "appointments") {

            filterStateParams = filterStateParams.filter(app => {
              const endDate = new Date(app.end),
                startDate = new Date(app.start);
              let thisIsObject = "";

              if (filterStartDate <= endDate && startDate <= filterEndDate) {

                thisIsObject = Object.values(app);
              }
              return thisIsObject;
            });
          }
        }
        break;
      default:
        filterStateParams;
    }
  });
  return filterStateParams;
}
