export default {
  // eslint-disable-next-line no-unused-vars
  install: (app, options) => {
    app.mixin({
      watch: {
        htmlPageTitle (val) {
          this.setHtmlPageTitle(val);
        }
      },
      mounted () {
        this.setHtmlPageTitle(this.htmlPageTitle);
      },
      methods: {
        setHtmlPageTitle (title) {
          if (title !== undefined) {
            document.title = title + (title.length ? " / " : "") + "DIPAS navigator";
          }
        }
      }
    });
  }
};
