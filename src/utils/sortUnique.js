/**
 * Generates a unique array of objects ordered using localeCompare
 * @param {Array} data Source data array of objects
 * @param {String} sortKey Name of the key used to sort the data
 * @return {Array} Array of objects
 */
export function sortUnique (data, sortKey = "name") {

  // Get lower case keys to make it case-insensitive
  let processedData = data.map(obj => obj[sortKey].toLowerCase());

  // Make keys unique
  processedData = Array.from(new Set(processedData));

  // Sort data using localeCompare
  processedData.sort((a, b) => a.localeCompare(b, "de", {sensitivity: "base"}));

  // Re-assign objects to array
  processedData = processedData.map(name => {
    return data.find(obj => obj[sortKey].toLowerCase() === name);
  });

  return processedData;

}
